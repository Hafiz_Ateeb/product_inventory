/**
 * Created by Hafiz Ateeb on 2/15/2016.
 */
var products = require('../../app/controllers/product'),
    config = require('../../config/config'),
    json2csv = require('json2csv'),
    winston = require('../../config/winston'),
    db = require('../../config/sequelize'),
    multer = require('multer'),
    upload = multer({dest: config.root + '/public/pics/'}),
    uploads = multer({dest: config.root + '/public/files/'});
var fields = ['upc', 'name', 'price'],
    fieldNames = ['Universal Code', 'Name', 'Price'],
    fs = require('fs');


var csv = require('csv-write-stream');
//var writer = csvWriter();

module.exports = function (app) {

    app.route('/product/create')
        .get(products.all)
        .post(upload.single('file'), products.create);

    app.route('/product/create/:id')
        .get(products.show)
        .delete(products.destroy)
        .put(upload.single('file'), products.update);


    app.post('/product/file', uploads.single('file'), function (req, res) {

        var columns = ["upc", "name", "price"];
        require('csv-to-array')({
            file: req.file.path,
            columns: columns
        }, function (err, array) {
            if (err) {
                throw err;
            } else {
                console.log(array);

                for (var i = 1; i < array.length; i++) {
                    checkValues(i, array[i]);
                }
            }
        });

        function checkValues(index, val) {
            if (val.upc.length == 9) {
                //console.log('looks bad');
                db.Product.find({where: {upc: val.upc}}, {attributes: ['upc', 'name', 'price']}).then(function (prodcts) {
                    if (prodcts) {
                        if (prodcts.price != val.price) {
                            db.Product.update({price: val.price}, {where: {upc: val.upc}}).then(function () {
                                winston.info('Record Updated at line ' + index);
                            });
                        }
                    }
                });
            } else {
                winston.warn('UPC is not valid at line ' + index);
            }
        }
    });

    app.get('/product/download', function (req, res) {
        db.Product.findAll().then(function (prodcts) {
            json2csv({data: prodcts, fields: fields, fieldNames: fieldNames}, function (err, csv) {
                if (err) {
                    throw err;
                } else {
                    fs.writeFile('index.csv', csv, function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('Done...!!');
                            var data = csv;
                            res.set('Content-Type', 'text/plain');
                            res.download(config.root+'/index.csv');
                            //res.setHeader('Content-Type', 'text/csv');
                            //res.end(data);
                            //res.end();
                        }
                    })
                }
            });
        })
    })
};