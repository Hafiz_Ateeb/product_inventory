/**
 * Created by Hafiz Ateeb on 2/15/2016.
 */
module.exports = function (sequelize, DataTypes) {
    var Product = sequelize.define('Product', {
        upc: DataTypes.INTEGER,
        name: DataTypes.STRING,
        price: DataTypes.STRING,
        image: DataTypes.STRING
    });

    return Product;
};