/**
 * Created by Hafiz Ateeb on 2/15/2016.
 */

var db = require('../../config/sequelize');

exports.all = function (req, res) {

    db.Product.findAll().then(function (prodcts) {

        return res.jsonp(prodcts);
    }).catch(function (err) {
        return res.render('error', {
            error: err,
            status: 500
        })
    })
};

exports.create = function (req, res) {


    db.Product.max('upc').then(function (max) {
        req.body.image = '../../pics/' + req.file.filename;
        req.body.upc = max + 1;
        db.Product.create(req.body).then(function (prodcts) {
            return res.jsonp(prodcts);
        }).catch(function (err) {
            return res.render('error', {
                error: err,
                status: 500
            })
        })
    });


};

exports.destroy = function (req, res) {
    var prodct = req.params.id;

    db.Product.destroy({where: {id: prodct}}).then(function (prodcts) {
        return res.jsonp(prodcts);
    }).catch(function (err) {
        return res.render('error', {
            error: err,
            status: 500
        })
    })
};

exports.show = function (req, res) {
    var prodct = req.params.id;

    db.Product.find({where: {id: prodct}}).then(function (prodcts) {
        return res.jsonp(prodcts);
    }).catch(function (err) {
        return res.render('error',{
            error: err,
            status: 500
        })
    })
};

exports.update = function (req, res) {
  var prodct = req.params.id;
    console.log(prodct + '=====================' + req.file);
    db.Product.update({
        upc: req.body.upc,
        name: req.body.name,
        price: req.body.price,
        image: '../../pics/' + req.file.filename
    }, {where: {id: prodct}}).then(function (prodcts) {
        if(!prodcts){
            console.log('The product is not updated...');
        }
        return res.jsonp(prodcts);
    }).catch(function (err) {
        return res.render('error', {
            error: err,
            status: 500
        })
    })
};