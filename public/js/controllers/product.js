/**
 * Created by Hafiz Ateeb on 2/15/2016.
 */
angular.module('mean').controller('ProductController', ['$scope', 'Global', 'Upload', 'Products', '$window', function ($scope, Global, Upload, Products, $window) {
    $scope.global = Global;

    $scope.butn = {
        hide: false
    };

    $scope.error = {
        hide: false
    };

    $scope.upload = {
        hide: false
    };

    $scope.size = {
        hide: false
    };

    Products.download().then(function (response) {

     var data = response.data,
     blob = new Blob([data], { type: 'text/plain' }),
     url = $window.URL || $window.webkitURL;
     $scope.fileUrl = url.createObjectURL(blob);
     });



    $scope.create = function (prodct, file) {
        file.upload = Upload.upload({
            url: '/product/create',
            data: {upc: prodct.upc, name: prodct.name, price: prodct.price, file: file}
        });

        location.reload('/product/create');

    };

    $scope.reset = function () {
        location.reload('/product/create');
    };


    $scope.find = function () {
        Products.get().then(function (response) {
            console.log(response);
            $scope.products = response.data;
        })
    };

    $scope.remove = function (id) {
        Products.delete(id).then(function (response) {
            console.log(response);
            location.reload('/product/create');
        })
    };

    $scope.editProdcts = function (prodct) {
        $scope.prodct = prodct;
        $scope.butn = {
            hide: true
        }
    };

    $scope.updateProdct = function (prodct, file) {
        console.log(file);
        if(!file){
            $scope.error = {
                hide: true
            }
        }else{
            file.upload = Upload.upload({
                url: '/product/create/' + prodct.id,
                method: 'PUT',
                data: {upc: prodct.upc, name: prodct.name, price: prodct.price, file: file}
            });

            location.reload('/product/create');
        }

    };

    /*$scope.downloadFile = function () {
        Products.download().then(function (response) {

            var data = response.data,
                blob = new Blob([data], { type: 'text/plain' }),
                url = $window.URL || $window.webkitURL;
            $scope.fileUrl = url.createObjectURL(blob);
        });
    };*/

    $scope.uploadFile = function (file) {
        if(file.name.split('.')[1] == 'csv'){
            if(file.size < '10000000'){
                file.upload = Upload.upload({
                    url: '/product/file',
                    data: {file: file}
                }).success(function (data) {
                    console.log(data + 'Uploaded');
                });
                location.reload('/product/create');
                console.log(file);
            }else{
                $scope.size = {
                    hide: true
                }
            }

        }else{
            $scope.upload = {
                hide: true
            }
        }

    };


}]);