/**
 * Created by Hafiz Ateeb on 2/15/2016.
 */
angular.module('mean').factory('Products', ['$http', function ($http) {
    return {
        get: function () {
            return $http.get('/product/create').then(function (response) {
                return response;
            })
        },
        delete: function (id) {
            return $http.delete('/product/create/' + id).then(function (response) {
                return response;
            })
        },
        download: function () {
            return $http.get('/product/download').then(function (response) {
                return response;
            })
        }
    }
}]);